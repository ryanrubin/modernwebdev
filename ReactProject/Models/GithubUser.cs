﻿using System;
using System.Collections.Generic;

using Microsoft.EntityFrameworkCore;
using System.Text.Json.Serialization;

namespace ReactProject.Models
{
    [Serializable]
    [Index(nameof(Login))]
    public class GithubUser
    {
        public Guid Id { get; set; }

        public string Login { get; set; }

        [JsonPropertyName("avatar_url")]
        public string AvatarUrl { get; set; }

        public string Type { get; set; }

        [JsonPropertyName("site_admin")]
        public bool? IsSiteAdmin { get; set; }

        public DateTime CreatedTimestamp { get; set; }



        public List<Post> Posts { get; set; }
    }

    public class Post
    {
        public int PostId { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }



        public Guid GithubUserId { get; set; }

        public GithubUser GithubUser { get; set; }
    }
}
