using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using Microsoft.EntityFrameworkCore;
using ReactProject.Biz;
using ReactProject.Db;
using ReactProject.Models;
using ReactProject.Services;

namespace ReactProject
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<GithubUserDbContext>(opt => opt.UseInMemoryDatabase("GithubUserDb"), ServiceLifetime.Singleton);
            //services.AddDbContext<GithubUserDbContext>(opt => opt.UseSqlServer(Configuration["GithubUserDbContext"]), ServiceLifetime.Singleton);
            services.AddSingleton<GithubUserBiz>();



            // Dependency Injection sample
            services.AddSingleton<IFlatDatabase<GithubUser>, FlatFileDatabase<GithubUser>>();
            //services.AddSingleton<IFlatDatabase<GithubUser>, FlatRecordDatabase<GithubUser>>();



            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen();



            services.AddControllersWithViews();

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(
            IApplicationBuilder app,
            IWebHostEnvironment env,
            IConfiguration cfg,
            IFlatDatabase<GithubUser> githubUserFd,
            GithubUserDbContext githubUserDbContext,
            GithubUserBiz githubUserBiz
            )
        {
            if (false)
            {
                // Step 1. Add-Migration InitialCreate

                // Step 2. Update-Database OR
                githubUserDbContext.Database.Migrate();
            }
            else
            {
                githubUserDbContext.Database.EnsureDeleted();
                githubUserDbContext.Database.EnsureCreated();
            }
            githubUserBiz.GithubUserDbContext = githubUserDbContext;



            // Configure Injected object
            //githubUserFd.SetLocation(cfg["FlatFileDatabase"]);
            //githubUserFd.SetLocation(cfg["FlatRecordDatabase"]);
            //githubUserFd.Load();



            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });



            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }
    }
}
