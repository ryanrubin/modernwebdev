﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using ReactProject.Models;

namespace ReactProject.Db
{
    public class GithubUserDbContext : DbContext
    {
        public GithubUserDbContext(DbContextOptions<GithubUserDbContext> options) : base(options) { }

        public DbSet<GithubUser> GithubUsers { get; set; }
        public DbSet<Post> Posts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Data seeding
            modelBuilder.Entity<GithubUser>().HasData(new GithubUser
            {
                Id = Guid.Parse("575C7EE0-E486-45E6-8E0A-08F3843A3207"),
                Login = "admin",
                AvatarUrl = "https://yt3.ggpht.com/ytc/AKedOLQ1BaK56RDJi9Jr-VMQjm4PIQ7rvK6gNuRgiWbbmg=s88-c-k-c0x00ffffff-no-rj",
                Type = "User",
                IsSiteAdmin = true
            });
            modelBuilder.Entity<GithubUser>().HasData(new GithubUser
            {
                Id = Guid.Parse("092B8735-A591-4942-BC8A-113E2A614EBF"),
                Login = "default_org",
                AvatarUrl = "",
                Type = "Organization",
                IsSiteAdmin = false
            });
        }
    }
}
