﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ReactProject.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GithubUsers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Login = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    AvatarUrl = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Type = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsSiteAdmin = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GithubUsers", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "GithubUsers",
                columns: new[] { "Id", "AvatarUrl", "IsSiteAdmin", "Login", "Type" },
                values: new object[] { new Guid("575c7ee0-e486-45e6-8e0a-08f3843a3207"), "https://yt3.ggpht.com/ytc/AKedOLQ1BaK56RDJi9Jr-VMQjm4PIQ7rvK6gNuRgiWbbmg=s88-c-k-c0x00ffffff-no-rj", true, "admin", "User" });

            migrationBuilder.InsertData(
                table: "GithubUsers",
                columns: new[] { "Id", "AvatarUrl", "IsSiteAdmin", "Login", "Type" },
                values: new object[] { new Guid("092b8735-a591-4942-bc8a-113e2a614ebf"), "", false, "default_org", "Organization" });

            migrationBuilder.CreateIndex(
                name: "IX_GithubUsers_Login",
                table: "GithubUsers",
                column: "Login");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GithubUsers");
        }
    }
}
