﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.Extensions.Configuration;
using ReactProject.Biz;
using ReactProject.Models;
using ReactProject.Services;

namespace ReactProject.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GithubUserController : ControllerBase
    {
        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IConfiguration _configuration;
        //private readonly IFlatDatabase<GithubUser> _githubUserFd;
        private readonly GithubUserBiz _githubUserBiz;

        public GithubUserController(
            ILogger<WeatherForecastController> logger,
            IConfiguration cfg,
            //IFlatDatabase<GithubUser> fd,
            GithubUserBiz githubUserBiz
            )
        {
            _logger = logger;
            _configuration = cfg;
            //_githubUserFd = fd;
            _githubUserBiz = githubUserBiz;
        }

        [HttpGet]
        public IEnumerable<GithubUser> Get()
        {
            return _githubUserBiz.List();
        }

        [HttpGet("{id}")]
        public GithubUser Get(Guid id)
        {
            return _githubUserBiz.Read(id);
        }

        [HttpPost]
        public GithubUser Post([FromBody] GithubUser user)
        {
            return _githubUserBiz.Create(user);
        }

        [HttpPut("{id}")]
        public GithubUser Put(Guid id, [FromBody] GithubUser user)
        {
            user.Id = id;
            return _githubUserBiz.Update(user);
        }

        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            _githubUserBiz.Delete(id);
        }

        [HttpPost("{id}/send-welcome-email")]
        public void PostSendWelcomeEmail(Guid id)
        {
        }

        // Old implementation below, not used anymore

        /*
        [HttpGet]
        public IEnumerable<GithubUser> Get()
        {
            _logger.LogWarning($"GithubUserController.Get was called on {DateTime.Now}.");

            _githubUserFd.Load();
            return _githubUserFd.GetRecords();
        }

        [HttpGet("{id}")]
        public GithubUser Get(Guid id)
        {
            _githubUserFd.Load();
            return _githubUserFd.GetRecords().FirstOrDefault(u => u.Id == id);
        }

        [HttpPost]
        public GithubUser Post([FromBody] GithubUser user)
        {
            user.Id = Guid.NewGuid();
            _githubUserFd.GetRecords().Add(user);
            _githubUserFd.Save();
            return user;
        }

        [HttpPut("{id}")]
        public GithubUser Put(Guid id, [FromBody] GithubUser user)
        {
            GithubUser userRecord = _githubUserFd.GetRecords().FirstOrDefault(u => u.Id == id);
            if (user.Login != null)
            {
                userRecord.Login = user.Login;
            }
            if (user.AvatarUrl != null)
            {
                userRecord.AvatarUrl = user.AvatarUrl;
            }
            if (user.Type != null)
            {
                userRecord.Type = user.Type;
            }
            if (user.IsSiteAdmin.HasValue)
            {
                userRecord.IsSiteAdmin = user.IsSiteAdmin;
            }
            _githubUserFd.Save();
            return userRecord;
        }

        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            GithubUser userRecord = _githubUserFd.GetRecords().FirstOrDefault(u => u.Id == id);
            _githubUserFd.GetRecords().Remove(userRecord);
            _githubUserFd.Save();
        }
        */
    }
}
