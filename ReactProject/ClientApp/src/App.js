import React, { useEffect } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';

import * as store from './store';

import Home from './components/Home';
import Login from './components/Login';
import GithubUsers from './components/GithubUsers';
import Blank from './components/Blank';

const App = () => {
    const dispatch = useDispatch();

    const isLoggedIn = useSelector(state => state.login.isLoggedIn);
    const loginDateTime = useSelector(state => state.login.loginDateTime);
    const trueValue = useSelector(state => state.login.trueValue);

    useEffect(() => {
        dispatch(store.getGithubUsersThunk());
    }, []);

    return <>
        <Router>
            <div className="my-2 container">
                {!isLoggedIn &&
                    <div className="row">
                        <div className="col-md-8">
                            <Home />
                        </div>
                        <div className="col-md-4">
                            <Login />
                        </div>
                    </div>
                }
                {isLoggedIn &&
                    <>
                        <div className="row">
                            <div className="col">
                                <div className="d-flex">
                                    <div className="p-1" style={{ flex: 1 }}>
                                        <small className="text-muted">Login date/time: {loginDateTime && loginDateTime.toString()}</small>
                                    </div>
                                    <div className="p-1">
                                        <small className="text-muted">True value: {trueValue ? 'true' : 'false'}</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="row mt-3">
                            <div className="col">
                                <div className="btn-group">
                                    <Link className="btn btn-outline-primary" to="/">Github Users</Link>
                                    <Link className="btn btn-outline-primary" to="/blank">Blank</Link>
                                </div>
                            </div>
                        </div>
                        <div className="row mt-3">
                            <div className="col">
                                <Switch>
                                    <Route path="/blank">
                                        <Blank />
                                    </Route>
                                    <Route path="/">
                                        <GithubUsers />
                                    </Route>
                                </Switch>
                            </div>
                        </div>
                    </>
                }
            </div>
        </Router>
    </>;
};

export default App;