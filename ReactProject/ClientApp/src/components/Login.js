﻿import React, { useState } from 'react';
import { useDispatch } from 'react-redux';

const Login = () => {
    const dispatch = useDispatch();

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [isShowPassword, setIsShowPassword] = useState(false);

    const logIn = () => {
        alert(`Username: ${username}\nPassword: ${password}`);
        dispatch({ type: 'login/loggedIn' });
        //dispatch({ type: 'login/isLoggedInSet', payload: true });
    };

    return <>
        <div className="card">
            <div className="p-1 card-header">
                <div className="p-1">
                    <h4>Log in here</h4>
                </div>
            </div>
            <div className="p-1 card-body">
                <div className="p-1">
                    <label htmlFor="username">Username</label>
                </div>
                <div className="p-1">
                    <input type="text" id="username" className="form-control" value={username} onChange={(e) => { setUsername(e.target.value); }} />
                </div>
                <div className="p-1">
                    <label htmlFor="password">Password</label>
                </div>
                <div className="d-flex">
                    <div className="p-1" style={{ flex: 1 }}>
                        <input type={isShowPassword ? 'text' : 'password'} id="password" className="form-control" value={password} onChange={(e) => { setPassword(e.target.value); }} />
                    </div>
                    <div className="p-1">
                        <button type="button" className="btn btn-secondary" style={{ minWidth: '5rem' }} onClick={() => { setIsShowPassword(!isShowPassword); }}>{isShowPassword ? 'Hide' : 'Show'}</button>
                    </div>
                </div>
                <div className="p-1">
                    <button type="button" className="btn btn-primary" onClick={logIn}>Log in</button>
                </div>
            </div>
        </div>
    </>;
};

export default Login;