﻿import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import $ from 'jquery';

import * as store from '../store';

import GithubUsersModal from './GithubUsersModal';

const GithubUsers = () => {
    const githubUsers = useSelector(state => state.githubUsers);
    const dispatch = useDispatch();

    const [isShowModal, setIsShowModal] = useState(false);
    const [modalMode, setModalMode] = useState('');
    const [modalUserId, setModalUserId] = useState(0);

    useEffect(() => {
        $('#githubUsersModal').modal({ show: false });
    }, []);

    useEffect(() => {
        if (isShowModal) {
            $('#githubUsersModal').modal('show');
        } else {
            $('#githubUsersModal').modal('hide');
            $('.modal-backdrop').remove();
            $('body').removeClass('modal-open');
        }
    }, [isShowModal]);

    const createUser = () => {
        setIsShowModal(true);
        setModalMode('Create');
    };

    const updateUser = (id) => {
        setIsShowModal(true);
        setModalMode('Update');
        setModalUserId(id);
    };

    const deleteUser = (id) => {
        if (window.confirm('Confirm delete?')) {
            //dispatch(store.deleteGithubUser({ id }));
            dispatch(store.deleteGithubUserThunk({ id }));
        }
    };

    return <>
        <div className="card">
            <div className="p-1 card-header">
                <div className="p-1">
                    <h4>Github Users</h4>
                </div>
            </div>
            <div className="p-1 card-body">
                <div className="p-1">
                    <button type="button" className="btn btn-primary" onClick={createUser}>Create</button>
                </div>
                <div className="p-1">
                    <table className="table" style={{ tableLayout: 'fixed' }}>
                        <thead>
                            <tr>
                                <th>Login</th>
                                <th>Avatar</th>
                                <th>Type</th>
                                <th>Is Site Admin?</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {githubUsers.map((user) => {
                                const { id, login, avatar_url, type, site_admin } = user;
                                return <tr key={id}>
                                    <td>{login}</td>
                                    <td><img src={avatar_url} style={{ width: '5rem', height: '5rem' }} /></td>
                                    <td>{type}</td>
                                    <td><input type="checkbox" disabled checked={site_admin} /></td>
                                    <td>
                                        <button type="button" className="btn btn-primary" onClick={() => { updateUser(id); }}>Update</button>
                                        <button type="button" className="btn btn-danger ml-1" onClick={() => { deleteUser(id); }}>Delete</button>
                                    </td>
                                </tr>;
                            })}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        {isShowModal &&
            <GithubUsersModal setIsShowModal={setIsShowModal} modalMode={modalMode} modalUserId={modalUserId} />
        }
    </>;
};

export default GithubUsers;