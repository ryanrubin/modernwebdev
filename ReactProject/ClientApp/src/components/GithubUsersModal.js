﻿import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import * as store from '../store';

const GithubUsersModal = ({ setIsShowModal, modalMode, modalUserId }) => {
    const githubUsers = useSelector(state => state.githubUsers);
    const dispatch = useDispatch();

    const [login, setLogin] = useState('');
    const [avatarUrl, setAvatarUrl] = useState('https://yt3.ggpht.com/ytc/AKedOLQGPnvqCK6g1L4qCX3fbiHMvbn1bYbJFyka14rSrQ=s88-c-k-c0x00ffffff-no-rj');
    const [type, setType] = useState('User');
    const [isSiteAdmin, setIsSiteAdmin] = useState(false);

    useEffect(() => {
        switch (modalMode) {
            case 'Update':
                const { login, avatar_url, type, site_admin } = githubUsers.filter(({ id }) => id === modalUserId)[0];
                setLogin(login);
                setAvatarUrl(avatar_url);
                setType(type);
                setIsSiteAdmin(site_admin);
                break;
        }
    }, []);

    const saveUser = () => {
        switch (modalMode) {
            case 'Create':
                //dispatch(store.createGithubUser({ login, avatar_url: avatarUrl, type, site_admin: isSiteAdmin }));
                dispatch(store.createGithubUserThunk({ login, avatar_url: avatarUrl, type, site_admin: isSiteAdmin }));
                break;
            case 'Update':
                //dispatch(store.updateGithubUser({ id: modalUserId, login, avatar_url: avatarUrl, type, site_admin: isSiteAdmin }));
                dispatch(store.updateGithubUserThunk({ id: modalUserId, login, avatar_url: avatarUrl, type, site_admin: isSiteAdmin }));
                break;
        }

        setIsShowModal(false);
    };

    return <>
        <div id="githubUsersModal" className="modal fade" tabIndex="-1">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h4>{modalMode} User</h4>
                    </div>
                    <div className="modal-body">
                        <div className="p-1">
                            <label htmlFor="login">Login</label>
                        </div>
                        <div className="p-1">
                            <input type="text" id="login" className="form-control" value={login} onChange={(e) => { setLogin(e.target.value); }} />
                        </div>
                        <div className="p-1">
                            <label htmlFor="avatarUrl">Avatar URL</label>
                        </div>
                        <div className="p-1">
                            <input type="text" id="avatarUrl" className="form-control" value={avatarUrl} onChange={(e) => { setAvatarUrl(e.target.value); }} />
                        </div>
                        <div className="p-1">
                            <label htmlFor="type">Type</label>
                        </div>
                        <div className="p-1">
                            <select id="type" className="form-control" value={type} onChange={(e) => { setType(e.target.value); }}>
                                <option value="User">User</option>
                                <option value="Organization">Organization</option>
                            </select>
                        </div>
                        <div className="p-1">
                            <label htmlFor="isSiteAdmin">Is Site Admin?</label>
                        </div>
                        <div className="p-1">
                            <input type="checkbox" id="isSiteAdmin" checked={isSiteAdmin} onChange={(e) => { setIsSiteAdmin(e.target.checked); }} />
                        </div>
                    </div>
                    <div className="modal-footer">
                        <div className="p-1">
                            <button type="button" className="btn btn-primary" onClick={saveUser}>Save</button>
                        </div>
                        <div className="p-1">
                            <button type="button" className="btn btn-primary" onClick={() => { setIsShowModal(false); }}>Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </>;
};

export default GithubUsersModal;