﻿import React from 'react';

const Blank = () => {
    return <>
        <div className="card">
            <div className="p-1 card-header">
                <div className="p-1">
                    <h4>Blank</h4>
                </div>
            </div>
        </div>
    </>;
};

export default Blank;