﻿import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';

const loginReducer = (state = {
    isLoggedIn: false,
    loginDateTime: null,
    trueValue: true
}, action) => {
    switch (action.type) {
        case 'login/loggedIn':
            return {
                ...state,
                isLoggedIn: true,
                loginDateTime: new Date()
            };
        case 'login/loggedOut':
            return {
                ...state,
                isLoggedIn: false,
                loginDateTime: null
            };
        case 'login/isLoggedInSet':
            return {
                ...state,
                isLoggedIn: action.payload,
                loginDateTime: new Date()
            };
        default:
            return state;
    }
};

const GITHUB_USERS_SET = 'githubUsers/set';
const GITHUB_USER_CREATE = 'githubUser/create';
const GITHUB_USER_UPDATE = 'githubUser/update';
const GITHUB_USER_DELETE = 'githubUser/delete';

let githubUsersId = 1000;

const githubUsersReducer = (githubUsers = [], action) => {
    const { type, payload } = action;
    switch (type) {
        case GITHUB_USERS_SET:
            return payload;
        case GITHUB_USER_CREATE:
            githubUsersId++;
            return [...githubUsers, { ...payload, id: githubUsersId }];
        case GITHUB_USER_UPDATE:
            return githubUsers.map((user) => {
                if (user.id === payload.id) {
                    return { ...user, ...payload };
                } else {
                    return user;
                }
            });
        case GITHUB_USER_DELETE:
            return githubUsers.filter(({ id }) => id !== payload.id);
        default:
            return githubUsers;
    }
};

// action creators
export const setGithubUsers = (githubUsers) => {
    return { type: GITHUB_USERS_SET, payload: githubUsers };
};
export const createGithubUser = (user) => {
    return { type: GITHUB_USER_CREATE, payload: user };
};
export const updateGithubUser = (user) => {
    return { type: GITHUB_USER_UPDATE, payload: user };
};
export const deleteGithubUser = (user) => {
    return { type: GITHUB_USER_DELETE, payload: user };
};

// thunks
export const getGithubUsersThunk = () => {
    return async (dispatch, getState) => {
        //const githubUsersFetch = await fetch('https://api.github.com/users');
        const githubUsersFetch = await fetch('/githubuser');
        const githubUsers = await githubUsersFetch.json();
        dispatch(setGithubUsers(githubUsers));
    };
};
export const createGithubUserThunk = (user) => {
    return async (dispatch, getState) => {
        await fetch('/githubuser', { method: 'POST', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify(user) });
        dispatch(createGithubUser(user));
    };
};
export const updateGithubUserThunk = (user) => {
    return async (dispatch, getState) => {
        await fetch(`/githubuser/${user.id}`, { method: 'PUT', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify(user) });
        dispatch(updateGithubUser(user));
    };
};
export const deleteGithubUserThunk = (user) => {
    return async (dispatch, getState) => {
        await fetch(`/githubuser/${user.id}`, { method: 'DELETE' });
        dispatch(deleteGithubUser(user));
    };
};

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    combineReducers({
        login: loginReducer,
        githubUsers: githubUsersReducer
    }),
    composeEnhancers(applyMiddleware(thunk))
);

export default store;