﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace ReactProject.Services
{
    public class FlatFileDatabase<T> : IFlatDatabase<T>
    {
        private readonly string fileName = $"{typeof(T).Name}.dat";

        private List<T> records = new List<T>();

        private string location;

        public void SetLocation(string loc)
        {
            location = loc;
        }

        public List<T> GetRecords()
        {
            return records;
        }

        public void Load()
        {
            if (!File.Exists(Path.Combine(location, fileName)))
            {
                return;
            }
            BinaryFormatter bf = new BinaryFormatter();
            using (FileStream fs = new FileStream(Path.Combine(location, fileName), FileMode.Open, FileAccess.Read))
            {
                records = (List<T>)bf.Deserialize(fs);
                fs.Close();
            }
        }

        public void Save()
        {
            if (!Directory.Exists(location))
            {
                Directory.CreateDirectory(location);
            }
            BinaryFormatter bf = new BinaryFormatter();
            using (FileStream fs = new FileStream(Path.Combine(location, fileName), FileMode.Create, FileAccess.Write))
            {
                bf.Serialize(fs, records);
                fs.Close();
            }
        }
    }
}
