﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactProject.Services
{
    public interface IFlatDatabase<T>
    {
        void SetLocation(string location);

        List<T> GetRecords();

        void Load();

        void Save();
    }
}
