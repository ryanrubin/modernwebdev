﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Text.Json;

namespace ReactProject.Services
{
    public class FlatRecordDatabase<T> : IFlatDatabase<T>
    {
        private List<T> records = new List<T>();

        private string location;

        public void SetLocation(string loc)
        {
            location = loc;
        }

        public List<T> GetRecords()
        {
            return records;
        }

        public void Load()
        {
            string data = "";

            using (SqlConnection conn = new SqlConnection(location))
            using (SqlCommand comm = new SqlCommand("select top 1 Data from GithubUser", conn))
            {
                conn.Open();
                using (SqlDataReader read = comm.ExecuteReader())
                {
                    if (read.Read())
                    {
                        data = (string)read["Data"];
                    }
                }
            }

            records = JsonSerializer.Deserialize<List<T>>(data);
        }

        public void Save()
        {
            string data = JsonSerializer.Serialize(records);

            using (SqlConnection conn = new SqlConnection(location))
            using (SqlCommand comm = new SqlCommand("update GithubUser set Data = @Data", conn))
            {
                conn.Open();
                comm.Parameters.AddWithValue("@Data", data);
                comm.ExecuteNonQuery();
            }
        }
    }
}
