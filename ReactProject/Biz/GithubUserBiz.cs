﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using ReactProject.Db;
using ReactProject.Models;

namespace ReactProject.Biz
{
    public class GithubUserBiz
    {
        public GithubUserDbContext GithubUserDbContext { get; set; }

        public IEnumerable<GithubUser> List()
        {
            var linqToEntities = GithubUserDbContext.GithubUsers.Take(1000);
            var linq = linqToEntities.ToList().Take(100);
            return linq;
        }

        public GithubUser Read(Guid id)
        {
            return GithubUserDbContext.GithubUsers.FirstOrDefault(u => u.Id == id);
        }

        public GithubUser Create(GithubUser user)
        {
            user.Id = Guid.NewGuid();
            GithubUserDbContext.GithubUsers.Add(user);
            GithubUserDbContext.SaveChanges();
            return user;
        }

        public GithubUser Update(GithubUser user)
        {
            GithubUser userRecord = GithubUserDbContext.GithubUsers.FirstOrDefault(u => u.Id == user.Id);
            if (user.Login != null)
            {
                userRecord.Login = user.Login;
            }
            if (user.AvatarUrl != null)
            {
                userRecord.AvatarUrl = user.AvatarUrl;
            }
            if (user.Type != null)
            {
                userRecord.Type = user.Type;
            }
            if (user.IsSiteAdmin.HasValue)
            {
                userRecord.IsSiteAdmin = user.IsSiteAdmin;
            }
            GithubUserDbContext.SaveChanges();
            return userRecord;
        }

        public void Delete(Guid id)
        {
            GithubUser userRecord = GithubUserDbContext.GithubUsers.FirstOrDefault(u => u.Id == id);
            GithubUserDbContext.GithubUsers.Remove(userRecord);
            GithubUserDbContext.SaveChanges();
        }
    }
}
